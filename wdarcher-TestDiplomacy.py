from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve, diplomacy_read, diplomacy_print


class TestDiplomacy (TestCase):
    def test_diplomacyRead_1(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        l = diplomacy_read(s)
        self.assertEqual(
            l, ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B']
        )
    def test_diplomacyRead_2(self):
        s = StringIO("A Austin Move Dallas\nD Dallas Hold\nM Miami Support A\nE ElPaso Move Miami\nO OU Support M\nH Houston Support E\n")
        l = diplomacy_read(s)
        self.assertEqual(
            l, ['A Austin Move Dallas', 'D Dallas Hold', 'M Miami Support A', 'E ElPaso Move Miami', 'O OU Support M', 'H Houston Support E']
        )
    def test_diplomacyRead_3(self):
        s = StringIO("C CorpusCristi Hold\nA Austin Move CorpusCristi\nG Georgia Move CorpusCristi\nS SanAntonio Support G\n")
        l = diplomacy_read(s)
        self.assertEqual(
            l, ['C CorpusCristi Hold', 'A Austin Move CorpusCristi', 'G Georgia Move CorpusCristi', 'S SanAntonio Support G']
        )
    def test_diplomacyPrint_1(self):
        out = []
        out.append(['A', '[dead]'])
        out.append(['B', 'Austin'])
        out.append(['D', 'Paris'])
        w = StringIO()
        diplomacy_print(w, out)
        self.assertEqual(w.getvalue(), 'A [dead]\nB Austin\nD Paris\n')

    def test_diplomacyPrint_2(self):
        out = []
        out.append(['A', 'America'])
        out.append(['C', 'Canada'])
        out.append(['D', 'Cuba'])
        w = StringIO()
        diplomacy_print(w, out)
        self.assertEqual(w.getvalue(), 'A America\nC Canada\nD Cuba\n')
    def test_diplomacyPrint_3(self):
        out = []
        out.append(['G', 'Guam'])
        out.append(['H', 'Hungry'])
        out.append(['I', '[dead]'])
        w = StringIO()
        diplomacy_print(w, out)
        self.assertEqual(w.getvalue(), 'G Guam\nH Hungry\nI [dead]\n')
    
    def test_diplomacySolve_1(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(s,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n"
        )
    def test_diplomacySolve_2(self):
        s = StringIO("A Austin Move Dallas\nD Dallas Hold\nM Miami Support A\nE ElPaso Move Miami\nO OU Support M\nH Houston Support E\n")
        w = StringIO()
        diplomacy_solve(s,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nD [dead]\nE [dead]\nH Houston\nM [dead]\nO OU\n"
        )
    def test_diplomacySolve_3(self):
        s = StringIO("C CorpusCristi Hold\nA Austin Move CorpusCristi\nG Georgia Move CorpusCristi\nS SanAntonio Support G\n")
        w = StringIO()
        diplomacy_solve(s,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nC [dead]\nG CorpusCristi\nS SanAntonio\n"
        )

if __name__ == '__main__':
    main()
""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out


$ coverage report -m                         >> TestDiplomacy.out


$ cat TestDiplomacy.out


"""