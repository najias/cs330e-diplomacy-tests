#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -------------
# TestDiplomacy
# -------------


class TestDiplomacy(TestCase):
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_2(self):
        r = StringIO("Z Austin Hold\nY Houston Move Austin\nX SanAntonio Support Z\nW Dallas Hold\nV FortWorth Move Austin\nU Lubbock Move Houston")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "U Houston\nV [dead]\nW Dallas\nX SanAntonio\nY [dead]\nZ Austin\n")

    def test_solve_3(self):
        r = StringIO("A Austin Hold\nK Kyoto Support A\nF Frankfurt Move Austin\nG Gatlinburg Support F\nN Nashville Support A\nS Sevilla Support F\nM Memphis Move Nashville")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nF Austin\nG Gatlinburg\nK Kyoto\nM [dead]\nN [dead]\nS Sevilla\n")
    
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold"
        input_line = diplomacy_read(s)
        self.assertEqual(input_line,  ['A', 'Madrid', 'Hold'])

    def test_read_2(self):
        s = "Z Rome Support W"
        input_line = diplomacy_read(s)
        self.assertEqual(input_line,  ['Z', 'Rome', 'Support', 'W'])

    def test_read_3(self):
        s = "M Madrid Move Barcelona"
        input_line = diplomacy_read(s)
        self.assertEqual(input_line,  ['M', 'Madrid', 'Move', 'Barcelona'])
    
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid'], ['D', 'Paris', 'Support', 'B']])
        self.assertEqual(v, ['A [dead]', 'B Madrid', 'C [dead]', 'D Paris'])

    def test_eval_2(self):
        v = diplomacy_eval([['Z', 'Austin', 'Support', 'D'], ['D', 'Dublin', 'Move', 'Austin'], ['F', 'Frankfurt', 'Support', 'D'], ['W', 'KansasCity', 'Move', 'Dublin']])
        self.assertEqual(v, ['D Austin', 'F Frankfurt', 'W Dublin', 'Z [dead]'])

    def test_eval_3(self):
        v = diplomacy_eval([['J', 'Phoenix', 'Move', 'Tempe'], ['K', 'Tempe', 'Move', 'SanDiego'], ['L', 'SanDiego', 'Move', 'Sacramento']])
        self.assertEqual(v, ['J Tempe', 'K SanDiego', 'L Sacramento'])
    
    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, ['J Tempe', 'K SanDiego', 'L Sacramento'])
        self.assertEqual(w.getvalue(), "J Tempe\nK SanDiego\nL Sacramento\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, ['D Austin', 'F Frankfurt', 'W Dublin', 'Z [dead]'])
        self.assertEqual(w.getvalue(), "D Austin\nF Frankfurt\nW Dublin\nZ [dead]\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, ['A [dead]', 'B Madrid', 'C [dead]', 'D Paris'])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

# ----
# main
# ----

if __name__ == "__main__": #pragma: no cover
    main()
