from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve, diplomacy_write, diplomacy_support, diplomacy_attack, \
	diplomacy_check


# -----------
# TestCollatz
# -----------


class TestDiplomacy(TestCase):
	# ----
	# read
	# ----
	
	def test_read(self):
		s = "A Madrid Hold"
		i = diplomacy_read(s)
		self.assertEqual(i, ['A', 'Madrid', 'Hold', '', 0, False, 'Madrid'])
	
	def test_read_2(self):
		s = "A Barcelona Move Madrid\n"
		i = diplomacy_read(s)
		self.assertEqual(i, ['A', 'Barcelona', 'Move', 'Madrid', 0, False, 'Barcelona'])
	
	def test_read_3(self):
		s = 'A Madrid Hold'
		i = diplomacy_read(s)
		self.assertEqual(i, ['A', 'Madrid', 'Hold', '', 0, False, 'Madrid'])
	
	# -----
	# print
	# -----
	
	def test_print(self):
		w = StringIO()
		diplomacy_write(w, [['A', 'Madrid', 'Hold', '', 0, False, 'Madrid']])
		self.assertEqual(w.getvalue(), "A Madrid\n")
	
	def test_print_2(self):
		w = StringIO()
		diplomacy_write(w, [['A', 'Madrid', 'Hold', '', 0, False, 'Madrid'],
		                    ['B', 'London', 'Move', 'Madrid', 0, False, 'London'],
		                    ['C', 'Austin', 'Support', 'B', 0, False, 'Austin']])
		self.assertEqual(w.getvalue(), "A Madrid\nB London\nC Austin\n")
	
	# -----
	# solve
	# -----
	
	def test_solve1(self):
		r = StringIO("A Madrid Hold\nB London Move Madrid\nC Austin Support B\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB Madrid\nC Austin\n")
	
	def test_solve2(self):
		r = StringIO("A Barcelona Move Madrid\nB London Move Madrid\nC Madrid Hold\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
	
	def test_solve3(self):
		r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\n')
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A Madrid\nB [dead]\nC London\n")


# ----
# main
# ----

main()
